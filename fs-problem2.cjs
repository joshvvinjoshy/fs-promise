const fs = require('fs');
// const path = require('path');
const pathh = '/home/joshvvinjoshy/mb/fscallbacks/';
function read_lipsum(fpath){
    let promise = new Promise((resolve, reject)=>{
        fs.readFile(fpath, 'utf-8', (err,data) =>{
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        });
    })
    return promise;
}
function toupper(data){
    const data_up = data.toUpperCase();
    // console.log(data_up);
    let promise = new Promise(function( resolve, reject){
        fs.writeFile(pathh+'lipsum_upper.txt', data_up, (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("Wrote data into lipsum_upper.txt");
            }
        });
    })
    return promise;
}
function append_lipsum_upper(){
    let promise = new Promise((resolve,reject) =>{
        fs.appendFile(pathh+'filenames.txt', 'lipsum_upper.txt,', (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("appended lipsum_upper.txt to filenames.txt");
            }
        });
    })
    return promise;
}
function readupper(){
    let promise = new Promise((resolve,reject) =>{
        fs.readFile(pathh + 'lipsum_upper.txt', 'utf-8', (err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        })
    })
    return promise;
}
function write_sentence(data){
    let promise = new Promise((resolve,reject) =>{
        const data_lower = data.toLowerCase();
        const sentenceRegex = /[^.!?]+[.!?]+/g;
        const sentences = data_lower.match(sentenceRegex);
        // console.log(sentences);
        const data_sentences = sentences.map(function (sentence){ 
                                            return sentence.trim();
                                        });
        // console.log(data_sentences);
        const data_sentences_str = data_sentences.toString();
        fs.writeFile(pathh +'lipsum_sentence.txt', data_sentences_str, (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("wrote data to lipsum_sentence.txt");
            }
        })
    })
    return promise;
}
function append_lipsum_sentence(){
    let promise = new Promise((resolve,reject) =>{
        fs.appendFile(pathh+'filenames.txt', 'lipsum_sentence.txt', (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("appended lipsum_sentence.txt to filenames.txt");
            }
        });
    })
    return promise;
}
function read_sentence(){
    let promise = new Promise((resolve,reject) =>{
        fs.readFile(pathh + 'lipsum_sentence.txt', 'utf-8', (err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        })
    })
    return promise;
}
function write_sorted(data){
    const data_list = data.split(',');
    data_list.sort();
    const sorted_data_list = data_list.join('\n');
    let promise = new Promise((resolve, reject) =>{
        fs.writeFile(pathh + 'sorted_lipsum_sentence.txt', sorted_data_list, (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("wrote data to sorted_lipsum_sentence.txt");
            }
        });
    })
    return promise;
}
function append_sorted_lipsum(){
    let promise = new Promise((resolve,reject) =>{
        fs.appendFile(pathh + 'filenames.txt', ',sorted_lipsum_sentence.txt', (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("appended sorted_lipsum_sentence.txt to filenames.txt");
            }
        });
    });
    return promise;
}
function read_filename(){
    let promise = new Promise((resolve,reject) =>{
        fs.readFile(pathh+'filenames.txt', 'utf-8', (err,data)=>{
            if(err){
                reject(err);
            }
            else{
                resolve(data);
            }
        })
    });
    return promise;
}
function remove_files(data){
    let promises = [];
    const arr = data.split(',');    
    arr.map((filename)=>{
        let promise = new Promise((resolve, reject)=>{
            fs.rm(pathh + filename, (err)=>{
                if(err){
                    reject(err);
                }
                else{
                    resolve(`${filename} is removed successfully`);
                }
            })
        })
        promises.push(promise);
    })
    return Promise.all(promises);
}
function convert(fpath){
    read_lipsum(fpath)
    .then((data)=>{
        return toupper(data);
    })                
    .then((message)=>{
        console.log(message);
        return append_lipsum_upper();
    })
    .then((message) =>{
        console.log(message);
        return readupper();
    })
    .then((data)=>{
        return write_sentence(data);
    })
    .then((message)=>{
        console.log(message);
        return append_lipsum_sentence();
    })
    .then((message) =>{
        console.log(message);
        return read_sentence();
    })
    .then((data) =>{
        return write_sorted(data);
    })
    .then((message)=>{
        console.log(message);
        return append_sorted_lipsum();
    })
    .then((message)=>{
        console.log(message);
        return read_filename();
    })
    .then((data)=>{
        return remove_files(data);
    })
    .then((messages)=>{
        messages.map((message)=>{
            console.log(message);
        })
    })
    .catch((err)=>{
        console.error(err);
    });
}
module.exports = convert;