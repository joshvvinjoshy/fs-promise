const fs = require('fs');
const path = require('path');
function deletedir(absolutePathOfRandomDirectory){
    let promise = new Promise(function(resolve,reject){
        fs.rmdir(absolutePathOfRandomDirectory, (err)=>{
            if(err){
                reject(err);
            }
            else{
                resolve("Directory deleted successfully");
            }
        })
    })
    return promise;
}
function deleteFile(files){;
    // let count = 0;
    let promises = [];
    for(const file in files){
        const filepath = files[file];
        let promise = new Promise(function(resolve, reject){
            fs.unlink(filepath, (err) =>{
                if(err){
                    reject(err);
                }
                else{
                    // count ++;
                    // console.log('deleted the json file', filepath)
                    resolve(`deleted the json file ${filepath}`);                          
                }
            });
        })
        promises.push(promise);
    }   
    return Promise.all(promises);
        // .then()
}
function createFile(absolutePathOfRandomDirectory, randomNumberOfFiles){
    let promises = [];
    for(let ind = 0; ind < randomNumberOfFiles; ind ++){
        const randdata = {
            id : ind,
            age : ind+1
        };
        const jsonData = JSON.stringify(randdata, null, 2);
        const filename = randdata['id'] +'.json';
        const filepath = path.join(absolutePathOfRandomDirectory, filename);
        let promise = new Promise(function(resolve,reject){
            fs.writeFile(filepath, jsonData, (err) =>{
                if(err){
                    reject(err);
                }
                else{                  
                    // console.log('json file created successfully',filename);
                    resolve(filepath);
                }             
            });
        })
        promises.push(promise);
    }
    return Promise.all(promises);
}
function createdir(absolutePathofRandomDirectory){
    let promise = new Promise((resolve,reject)=>{
        fs.mkdir(absolutePathofRandomDirectory, (err) =>{
            if(err){
                reject(err);
            }
            else{
                resolve("Directory created successfully");
            }
        });
    })
    return promise;
}
function fsProblem1(absolutePathofRandomDirectory, randomNumberOfFiles=1){
    createdir(absolutePathofRandomDirectory)
    .then(function(message){
        console.log(message);
        return createFile(absolutePathofRandomDirectory, randomNumberOfFiles)
    })
    .then(function(files){
        // console.log(files);
        files.map((file)=>{
            console.log(file,"created successfully");
        })
        return deleteFile(files);
    })
    .then(function(messages){
        // console.log(messages);
        messages.map((message)=>{
            console.log(message);
        })
        console.log("All the files are deleted");
        
        return deletedir(absolutePathofRandomDirectory);
    })
    .then(function(message){
        console.log(message);
    })
    .catch(function(error){
        console.error(error);
    })
}  
module.exports = fsProblem1;